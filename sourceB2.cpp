
#include "sqlite3.h"
#include <iostream>
#include <string>
#include <unordered_map>
#include <vector>

using namespace std;

int retInt;


bool carPurchase(int buyerid, int carid, sqlite3* db, char* zErrMsg);
bool balanceTransfer(int from, int to, int amount, sqlite3* db, char* zErrMsg);

unordered_map<string, vector<string>> results;
void clearTable()
{
	for (auto it = results.begin(); it != results.end(); ++it)
	{
		it->second.clear();
	}
	results.clear();
}

void printTable()
{
	auto iter = results.end();
	iter--;
	//	int size = results.begin()->second.size();
	int size = iter->second.size();
	for (int i = -2; i < size; i++)
	{
		for (auto it = results.begin(); it != results.end(); ++it)
		{
			if (i == -2)
			{
				//cout << it->first << " ";
				printf("|%*s|", 13, it->first.c_str());
			}
			else if (i == -1)
				cout << "_______________";
			else
				//	cout << it->second.at(i) << " ";
				printf("|%*s|", 13, it->second.at(i).c_str());
		}
		cout << endl;
	}
}

int callback(void* notUsed, int argc, char** argv, char** azCol)
{
	int i;

	for (i = 0; i < argc; i++)
	{
		auto it = results.find(azCol[i]);
		if (it != results.end())
		{
			it->second.push_back(argv[i]);
			retInt = atoi(argv[i]);
		}
		else
		{
			pair<string, vector<string>> p;
			p.first = azCol[i];
			p.second.push_back(argv[i]);
			results.insert(p);
			retInt = atoi(argv[i]);
		}
	}

	return 0;
}

int getLastId(void* notUsed, int argc, char** argv, char** azCol)
{
	cout << "the Last id is: " << argv[0] << endl;
	return 0;
}

int main()
{
	int rc;
	sqlite3* db;
	char *zErrMsg = 0;
	bool flag = true;

	bool flagBuy;
	bool flagTrans;
	

	// connection to the database
	rc = sqlite3_open("carsDealer.db", &db);

	if (rc)
	{
		cout << "Can't open database: " << sqlite3_errmsg(db) << endl;
		sqlite3_close(db);
		system("Pause");
		return(1);
	}


	cout << "Table cars before change:" << endl;
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");


	clearTable();

	cout << "Table accounts before change:" << endl;
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");


	flagBuy = carPurchase(1, 4, db, zErrMsg); //good
	if (flagBuy)
	{
		cout << "Purchase successful :)" << endl;
	}
	else
	{
		cout << "Purchase not succeeded :)" << endl;
	}
	flagBuy = carPurchase(12, 14, db, zErrMsg); //good
	if (flagBuy)
	{
		cout << "Purchase successful :)" << endl;
	}
	else
	{
		cout << "Purchase not succeeded :)" << endl;
	}
	flagBuy = carPurchase(2,2, db, zErrMsg); //not good
	if (flagBuy)
	{
		cout << "Purchase successful :)" << endl;
	}
	else
	{
		cout << "Purchase not succeeded :)" << endl;
	}
	system("Pause");
	system("CLS");
	clearTable();

	cout << "Table cars after change:" << endl;
	rc = sqlite3_exec(db, "select * from cars", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");


	clearTable();

	cout << "Table accounts after change:" << endl;
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	
	clearTable();

	cout << "Table accounts before change:" << endl;
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");

	flagTrans = balanceTransfer(2, 3, 4000,db, zErrMsg); //good
	if (flagTrans)
	{
		cout << "Transfer successful $-)" << endl;
	}
	else
	{
		cout << "Transfer not success $-(" << endl;
	}
	flagTrans = balanceTransfer(7, 10, 5000, db, zErrMsg); //good
	if (flagTrans)
	{
		cout << "Transfer successful $-)" << endl;
	}
	else
	{
		cout << "Transfer not success $-(" << endl;
	}
	flagTrans = balanceTransfer(1, 11, 44001, db, zErrMsg); // not good
	if (flagTrans)
	{
		cout << "Transfer successful $-)" << endl;
	}
	else
	{
		cout << "Transfer not success $-(" << endl;
	}

	system("Pause");
	system("CLS");

	clearTable();

	cout << "Table accounts after change:" << endl;
	rc = sqlite3_exec(db, "select * from accounts", callback, 0, &zErrMsg);

	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	else
	{
		printTable();
	}

	system("Pause");
	system("CLS");


	clearTable();



	sqlite3_close(db);

	system("Pause");

}


/*
This function will cheak if the purchase can be made
INPUT: id of the buyer, id of the car to buy
OUTPUT: true if the purchase can be made, false if not
*/
bool carPurchase(int buyerid, int carid, sqlite3 * db, char * zErrMsg)
{
	int rc;
	bool good = false;
	string str;

	str = "select available from cars where id=" + std::to_string(carid);

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}

	int available = retInt;
	if (available == 1) //good
	{
		str = "select price from cars where id=" + std::to_string(carid);

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		int carPrice = retInt;

		str = "select balance from accounts where id=" + std::to_string(buyerid);

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		int balanceBuyer = retInt;

		if (balanceBuyer > carPrice || balanceBuyer == carPrice) //can buy
		{
			rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}

			int newBalance = balanceBuyer - carPrice;
			str = "update accounts set balance=" + std::to_string(newBalance) + " where id=" + std::to_string(buyerid);

			rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}


			str = "update cars set available=0 where id=" + std::to_string(carid);

			rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);

			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}

			rc = sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
			if (rc != SQLITE_OK)
			{
				cout << "SQL error: " << zErrMsg << endl;
				sqlite3_free(zErrMsg);
				system("Pause");
				return 1;
			}
			good = true;

		}
		else //if the buyer dont have enough money
		{
			good = false;
		}
	}
	else //if the car not available
	{
		good = false;
	}



	return good;
}


/*
This function will cheak if transfer of money can be made
INPUT: id of the paying, id of the recive
OUTPUT: true if the transfer can be made, false if not
*/
bool balanceTransfer(int from, int to, int amount, sqlite3 * db, char * zErrMsg)
{
	int rc;
	bool good = false;
	string str;

	str = "select balance from accounts where id=" + std::to_string(from);

	rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
	if (rc != SQLITE_OK)
	{
		cout << "SQL error: " << zErrMsg << endl;
		sqlite3_free(zErrMsg);
		system("Pause");
		return 1;
	}
	int balanceFrom = retInt;

	if (balanceFrom > amount || balanceFrom == amount) //can transfer
	{
		rc = sqlite3_exec(db, "begin transaction", callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		int newBalanceFrom = balanceFrom - amount;
		str = "update accounts set balance=" + std::to_string(newBalanceFrom) + " where id=" + std::to_string(from);

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}

		str = "select balance from accounts where id=" + std::to_string(to);

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		int balanceTo = retInt;

		int newBalanceTo = balanceTo + amount;
		str = "update accounts set balance=" + std::to_string(newBalanceTo) + " where id=" + std::to_string(to);

		rc = sqlite3_exec(db, str.c_str(), callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}


		rc = sqlite3_exec(db, "commit", callback, 0, &zErrMsg);
		if (rc != SQLITE_OK)
		{
			cout << "SQL error: " << zErrMsg << endl;
			sqlite3_free(zErrMsg);
			system("Pause");
			return 1;
		}
		good = true;

	}
	else //if the paying dont hav enough money
	{
		good = false;
	}

	return good;
}
